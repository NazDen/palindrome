import java.util.TreeSet;

public class Main {

    private final static long[] simples = new long[9000];
    //    private final static long[] palindromes = new long[9000];
    private static int posSimple = 0;
    private static int posPalidrome = 0;
    private static long counterOpSimple = 0;
    private static long counterOpPalindrome = 0;

    static boolean isSimple(long x) {
        long i = 2;
        if (x % i == 0) return false;
        i++;
        long maxI = (long) Math.sqrt(x);
        while (i < maxI) {
            long rest = x % i;
            counterOpSimple++;
            if (rest == 0) {
                return false;
            }
            i = i + 2;
        }
        simples[posSimple++] = x;
        return true;
    }


    static boolean isPalindrome(Long i) {
        char[] palindrome = String.valueOf(i).toCharArray();
        int fromBegin = 0;
        int fromEnd = palindrome.length - 1;
        while (fromBegin <= fromEnd) {
            if (palindrome[fromBegin] == palindrome[fromEnd]) {
                counterOpPalindrome++;
                fromBegin++;
                fromEnd--;
            } else return false;
        }
        return true;
    }


    public static void main(String[] args) {
        //Simple
        long simpleCounter = 0;
        long t1 = System.currentTimeMillis();
        for (long s = 10_000; s < 1_00_000; ++s) {
            if (isSimple(s)) {
                simpleCounter++;
            }
        }
        long t2 = System.currentTimeMillis();
        long sTime = t2 - t1;
        System.out.printf("simples count:%d, operations:%d, time:%d millis", simpleCounter, counterOpSimple, sTime);
        //Simple

        //Palindrome
        TreeSet<Long> palindromes = new TreeSet<>();
        long palindromeCounter = 0;
        long a = 0, b = 0;
        long t3 = System.currentTimeMillis();
        for (int i = 0; i < posSimple; i++) {
            long simple = simples[i];
            for (int j = posSimple - 1; j >= i; j--) {
                long simple2 = simples[j];
                Long l = simple * simple2;
                if (isPalindrome(l)) {
                    palindromes.add(l);
                    palindromeCounter++;
                }
            }
        }
            long t4 = System.currentTimeMillis();
            long pTime = t4 - t3;
            System.out.printf("\npalindromes count:%d, operations:%d, time:%d millis, maxPol: %d", palindromeCounter, counterOpPalindrome, pTime, palindromes.last());
//            System.out.println();
//            palindromes.forEach(System.out::println);
            //Palindrome
    }
}



